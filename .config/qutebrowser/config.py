
black = "#000000"
blue = "#008b8b"
light_blue = "#83a598"
red = "#ff0000"
yellow = "#daa520"
green = "#8ec07c"

c.colors.completion.fg = blue
c.colors.completion.odd.bg = black
c.colors.completion.even.bg = black
c.colors.completion.category.fg = light_blue
c.colors.completion.category.bg = black
c.colors.completion.category.border.top = blue
c.colors.completion.category.border.bottom = blue
c.colors.completion.item.selected.fg = light_blue
c.colors.completion.item.selected.bg = black
c.colors.completion.item.selected.match.fg = yellow
c.colors.completion.item.selected.border.top = light_blue
c.colors.completion.item.selected.border.bottom = light_blue
c.colors.completion.match.fg = light_blue
c.colors.completion.scrollbar.fg = blue
c.colors.completion.scrollbar.bg = black

c.colors.downloads.bar.bg = black
c.colors.downloads.start.fg = light_blue
c.colors.downloads.start.bg = black
c.colors.downloads.stop.fg = light_blue
c.colors.downloads.stop.bg = black
c.colors.downloads.error.fg = red

c.colors.hints.fg = blue
c.colors.hints.bg = black
c.hints.border = '2px solid' + blue
c.colors.hints.match.fg = light_blue

c.colors.keyhint.fg = blue
c.colors.keyhint.suffix.fg = light_blue
c.colors.keyhint.bg = black

c.colors.messages.error.fg = red
c.colors.messages.error.bg = black
c.colors.messages.error.border = red
c.colors.messages.warning.fg = yellow
c.colors.messages.warning.bg = black
c.colors.messages.warning.border = yellow
c.colors.messages.info.fg = blue
c.colors.messages.info.bg = black
c.colors.messages.info.border = black

c.colors.prompts.fg = blue
c.colors.prompts.bg = black
c.colors.prompts.border = '1px solid' + blue
c.colors.prompts.selected.bg = blue

c.colors.statusbar.normal.fg = light_blue
c.colors.statusbar.normal.bg = black
c.colors.statusbar.insert.fg = light_blue
c.colors.statusbar.insert.bg = black
c.colors.statusbar.passthrough.fg = green
c.colors.statusbar.passthrough.bg = black
c.colors.statusbar.private.fg = yellow
c.colors.statusbar.private.bg = black
c.colors.statusbar.command.fg = light_blue
c.colors.statusbar.command.bg = black
c.colors.statusbar.command.private.fg = blue
c.colors.statusbar.command.private.bg = black
c.colors.statusbar.caret.fg = yellow
c.colors.statusbar.caret.bg = black
c.colors.statusbar.caret.selection.fg = light_blue
c.colors.statusbar.caret.selection.bg = black
c.colors.statusbar.progress.bg = blue
c.colors.statusbar.url.fg = blue
c.colors.statusbar.url.error.fg = red
c.colors.statusbar.url.hover.fg = blue
c.colors.statusbar.url.success.http.fg = light_blue
c.colors.statusbar.url.success.https.fg = light_blue
c.colors.statusbar.url.warn.fg = yellow

c.colors.tabs.bar.bg = black
c.colors.tabs.indicator.start = blue
c.colors.tabs.indicator.stop = blue
c.colors.tabs.indicator.error = red
c.colors.tabs.odd.fg = light_blue
c.colors.tabs.odd.bg = black
c.colors.tabs.even.fg = light_blue
c.colors.tabs.even.bg = black
c.colors.tabs.selected.odd.fg = blue
c.colors.tabs.selected.odd.bg = black
c.colors.tabs.selected.even.fg = blue
c.colors.tabs.selected.even.bg = black
